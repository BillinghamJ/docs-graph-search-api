Facebook Graph Search
=====================

Today, Facebook announced the ability to search the Facebook graph in a fluid & natural way. They provided a button which generates example personalised search results, so I decided to pull it apart and play with it.

The button has a standard-Facebook `ajaxify` attribute which provides a URL starting with `/ajax/browse/example_query.php`. It is a GET request.

The above URL is followed by a long complex string of parameters. Below, I have identified what each of these are and potential ways they could be altered.

Request
-------

### URL

`https://www.facebook.com/ajax/browse/example_query.php`

### Parameters

Below are all the parameters sent with the request. I have included all of the details from my personal URL as the only unique part is my user ID which is hardly a secret anyway - http://graph.fb.me/BillinghamJ

- `encoded_query[function][function_name]` => `residents`
- `encoded_query[function][params][0]` => `114952118516947`
- `encoded_query[grammar_version]` => `0a2531f2cef70597cc31cd94287819c5e3205ded`
- `text` => `People who live in San Francisco, California`
- `category` => `city`
- `__asyncDialog` => `4`
- `__user` => `100001025460375`
- `__a` => `1`
- `__req` => `9`

The API
-------

Pretty clearly, this is a fairly 'fudged' API. It's not designed to be overly pretty - it's designed to facilitate the initial showing off of a feature which isn't yet released, so I've pulled the bits apart and provided some details of how I've played with them.

If you send a request which the API doesn't understand, you receive a blank response and a `500` HTTP status. This is hereby known as the `lolwtf error`.

However, if you send a request which (presumably) isn't allowed to be used be we mere mortals yet, you receive a `404` HTTP status along with the following message...

`Not Found <br /><a href="http://www.facebook.com/">Back to Facebook.</a>`

...since this is well formatted & exact HTML, not a normal complex JSON response, I'm pretty sure it's intentional and not a normal `404` message. In reality, I suspect `403` would be more accurate. This is hereby known as the `GTFO message`.

Dissecting those parameters
---------------------------

The "encoded_query" bit appears to be a dictionary detailing information for making the actual request internally. From what I could tell, the other data isn't actually used for getting the results - I changed the "text" property which caused the header to change, but didn't change the actual results.

### Function name

- URL key: `encoded_query[function][function_name]`
- Current value: `residents`
- What is it? `Specifies what type of data you're looking for`
- Optional? `No`

#### Notes

I tried changing this in various ways with mixed results. Nothing alternative which I have tried yet has succeeded - I assume this is locked down to accept only `residents`.

#### Values

I have tried these values - let me know for new additions to this list

- `residents` - works perfectly (Y)
- `photos` - GTFO message (see above)
- `friends` - GTFO message
- `images` - lolwtf error (see above)
- `img` - lolwtf error
- `asdf` - lolwtf error
- `[blank]` - lolwtf error
- omitting parameter entirely - lolwtf error

### First function parameter

- URL key: `encoded_query[function][params][0]`
- Current value: `114952118516947`
- What is it? `Graph ID for the place to search`
- Optional? `No`

#### Notes

http://graph.fb.me/114952118516947

Appears to be locked down to `114952118516947` - San Francisco.

#### Values

- `114952118516947` - San Francisco, California - (Y)
- `105742132800057` - Warrington, UK - GTFO message
- `113468615330042` - San Luis Obispo, California - GTFO message
- `[non existent id]` - lolwtf error
- `1a14952118516947` - lolwtf error
- omitting parameter entirely - lolwtf error

### Grammar version

- URL key: `encoded_query[grammar_version]`
- Current value: `0a2531f2cef70597cc31cd94287819c5e3205ded`
- What is it? `SHA1 hash of something`
- Optional? `Yes`

#### Notes

Changing this in any way (including deleting it) does not appear to have any impact on the result of the query.

I suspect this is dictating which version of the grammar interpreter is used. Since the text parameter is written in a natural human way, converting it to something a computer can understand is complex. I assume this will also change with support for other languages, but (afaik) only en-US is supported.

I'm fairly sure this is a SHA1 hash in hexadecimal format based on its length and character contents. If someone could have a go at cracking it, it might be interesting to see what it was originally.

Also, I have checked that this isn't any kind of identifier. Doing a search on Twitter yields this Tweet as a result...

https://twitter.com/joelchornik/status/291255640561692672

...so I'm pretty certain it's the same for everyone.

#### Values

- `0a2531f2cef70597cc31cd94287819c5e3205ded` - (Y)
- `0a2531f2cef70597cc31cd94287819c5e3205dee` - (Y)
- `0a2531f2cef70597cc31cd94287819c5e3205de` - (Y)
- `[blank]` - (Y)
- omitting parameter entirely - (Y)

### Query text

- URL key: `text`
- Current value: `People who live in San Francisco, California`
- What is it? `Example text search`
- Optional? `Yes`

#### Notes

From playing with this, it's doesn't seem to be used at all. It is just put back out into the page in its exact form (though HTML escaped) as a header.

I suspect that the natural text part of graph search isn't in a stable state yet, hence fudging it by manually specifying the function stuff.

#### Values

- `People who live in San Francisco, California` - (Y)
- `people who live in San Francisco, California` - (Y)
- `herp, derp` - (Y)
- `[blank]` - (Y)
- omitting parameter entirely - (Y)

### Place category

- URL key: `category`
- Current value: `city`
- What is it? `Category of the place ID specified in First function parameter`
- Optional? `No`

#### Notes

Playing with this = death. From testing, nothing but "city" works.

I assume this is referring to & must match the place ID, but surely Fb could just infer it from the place ID? I suspect this is more fudging.

#### Values

- `city` - (Y)
- `town` - lolwtf error
- `country` - lolwtf error
- `restaurant` - lolwtf error
- `asdf` - lolwtf error
- `[blank]` - lolwtf error
- omitting parameter entirely - lolwtf error

### User ID

- URL key: `__user`
- Current value: `100001025460375`
- What is it? `Graph ID of user making the request`
- Optional? `Yes`

#### Notes

It's not possible to put another user's ID in there instead of your own.

#### Values

- `100001025460375` - (Y)
- `4` - error 1357032 `Sorry, we got confused`
- `736587265764981743958742` - error 1357032 `Sorry, we got confused`
- `100001025460375a` - error 1357005 `Your request couldn't be processed`
- `[blank]` - error 1357032 `Sorry, we got confused`
- omitting parameter entirely - (Y)

### __asyncDialog, __a, __req

[Happy to receive any corrections on this particularly]

Don't know what these are & inclined to think they're pretty irrelevant anyway. Likely a standard part of any `ajaxify` URL.

Omitting __asyncDialog or __req appears to make no difference.

Omitting __a causes the response to be blank, but it has a 200 HTTP status.

Misc.
-----

More than happy to accept pull requests to correct/add information.

&copy; James Billingham 2013; License: [Creative Commons BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/)
